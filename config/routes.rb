Rails.application.routes.draw do
  root 'main#info'
  get 'main/index'
  get 'main/switch'
  get 'main/bla'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
